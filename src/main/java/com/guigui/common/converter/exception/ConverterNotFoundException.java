package com.guigui.common.converter.exception;

import com.guigui.exception.InternalErrorException;
import static java.lang.String.format;

public class  ConverterNotFoundException extends InternalErrorException {
    public ConverterNotFoundException(Class<?> classType){
        super(format("Converter not found for type %s", classType.getName()));
    }
}
