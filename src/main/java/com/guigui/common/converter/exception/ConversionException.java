package com.guigui.common.converter.exception;

import com.guigui.exception.InternalErrorException;

import static java.lang.String.format;

public class ConversionException extends InternalErrorException {

    public ConversionException(String value, Class<?> targetType){
        super(formatMessage(value, targetType));
    }

    public ConversionException(String value, Class<?> targetType, Throwable cause){
        super(formatMessage(value, targetType), cause);
    }

    private static String formatMessage(String value, Class<?> targetType){
        return format("%s couldn't be converted to %s", value, targetType);
    }
}
