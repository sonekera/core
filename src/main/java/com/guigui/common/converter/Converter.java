package com.guigui.common.converter;

public interface Converter <T>{

    Class<T> getType();

    T convertTo(String value);

    String convertFrom(T value);
}
