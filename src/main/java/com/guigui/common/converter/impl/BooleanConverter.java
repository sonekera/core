package com.guigui.common.converter.impl;

import com.guigui.common.converter.Converter;
import com.guigui.common.converter.exception.ConversionException;

import static com.guigui.common.util.PreconditionsUtil.checkNotBlank;
import static com.guigui.common.util.PreconditionsUtil.checkNotNull;

public class BooleanConverter implements Converter<Boolean> {
    @Override
    public Class<Boolean> getType() {
        return Boolean.class;
    }

    @Override
    public Boolean convertTo(String value) {
        checkNotBlank(value);
        try{
            if("TRUE".equalsIgnoreCase(value)
                    || "1".equalsIgnoreCase(value)
                    || "YES".equalsIgnoreCase(value)
                    || "Y".equalsIgnoreCase(value)
            ){
                return Boolean.TRUE;
            }
            return Boolean.FALSE;
        }catch (Exception ex){
            throw new ConversionException(value, getType(), ex);
        }
    }

    @Override
    public String convertFrom(Boolean value) {
        return checkNotNull(value).toString();
    }
}
