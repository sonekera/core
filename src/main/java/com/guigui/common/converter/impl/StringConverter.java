package com.guigui.common.converter.impl;

import com.guigui.common.converter.Converter;

import static com.guigui.common.util.PreconditionsUtil.checkNotBlank;

public class StringConverter implements Converter<String> {
    @Override
    public Class<String> getType() {
        return String.class;
    }

    @Override
    public String convertTo(String value) {
       return checkNotBlank(value);
    }

    @Override
    public String convertFrom(String value) {
        return checkNotBlank(value);
    }
}
