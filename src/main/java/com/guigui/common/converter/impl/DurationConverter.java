package com.guigui.common.converter.impl;

import com.guigui.common.converter.Converter;
import com.guigui.common.converter.exception.ConversionException;

import java.time.Duration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.guigui.common.util.PreconditionsUtil.checkNotBlank;
import static com.guigui.common.util.PreconditionsUtil.checkNotNull;

public class DurationConverter implements Converter<Duration> {

    private static final Pattern DURATION_PATTERN = Pattern.compile("^(-?[0-9]+)(d|h|m|s|ms)$");

    private final Long DURATION_MILLIS = 1L;
    private final Long DURATION_SECONDS = DURATION_MILLIS * 1000L;
    private final Long DURATION_MINUTES = DURATION_SECONDS * 60L;
    private final Long DURATION_HOURS = DURATION_MINUTES * 60L;
    private final Long DURATION_DAYS = DURATION_HOURS * 24L;


    @Override
    public Class<Duration> getType() {
        return Duration.class;
    }

    @Override
    public Duration convertTo(String value) {
        checkNotBlank(value);
        if("0".equals(value)){
            return Duration.ZERO;
        }
        Matcher matcher = DURATION_PATTERN.matcher(value);
        if(matcher.matches()){
            var ttl = Integer.parseInt(matcher.group(1));
            var unit = matcher.group(2);

            switch (unit){
                case "d":
                    return Duration.ofDays(ttl);
                case "h":
                    return Duration.ofHours(ttl);
                case "m":
                    return Duration.ofMinutes(ttl);
                case "s":
                    return Duration.ofSeconds(ttl);
                default:
                    return Duration.ofMillis(ttl);
            }
        }
        throw new ConversionException(value, getType());

    }

    @Override
    public String convertFrom(Duration value) {
        if(checkNotNull(value).isZero()){
            return "0";
        }

        var sb = new StringBuilder();
        var time = value.toMillis();
        if(time < 0){
            sb.append("-");
            time = -time;
        }

        if(time % DURATION_SECONDS != 0){
            sb.append(time / DURATION_MILLIS).append("ms");
        }else if (time % DURATION_MINUTES != 0){
            sb.append(time / DURATION_SECONDS).append('s');
        }else if(time % DURATION_HOURS != 0){
            sb.append(time / DURATION_MINUTES).append('h');
        }else if(time % DURATION_DAYS != 0) {
            sb.append(time / DURATION_HOURS).append('h');
        }else{
            sb.append(time /DURATION_DAYS).append('d');
        }
        return sb.toString();
    }
}
