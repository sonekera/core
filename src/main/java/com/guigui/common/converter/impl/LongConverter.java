package com.guigui.common.converter.impl;

import com.guigui.common.converter.Converter;
import com.guigui.common.converter.exception.ConversionException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import static com.guigui.common.util.PreconditionsUtil.checkNotNull;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class LongConverter implements Converter<Long> {
    @Override
    public Class<Long> getType() {
        return Long.class;
    }

    @Override
    public Long convertTo(String value) {
        try{
            return Long.valueOf(value);
        }catch (Exception ex){
            throw new ConversionException(value, getType(), ex);
        }
    }

    @Override
    public String convertFrom(Long value) {
        return checkNotNull(value).toString();
    }
}
