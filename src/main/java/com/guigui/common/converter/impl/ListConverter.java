package com.guigui.common.converter.impl;

import com.guigui.common.converter.Converter;
import com.guigui.common.converter.exception.ConversionException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.guigui.common.CommonConstants.CSV_SEPARATOR;
import static com.guigui.common.util.PreconditionsUtil.checkNotBlank;
import static com.guigui.common.util.PreconditionsUtil.checkNotNull;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public class ListConverter<T> implements Converter<List<T>> {
    
    private final Converter<T> delegate;
    
    @Override
    public Class<List<T>> getType() {
        //TODO: implementar exceção customizada
        throw new RuntimeException("Invalid Implementation");
    }

    @Override
    public List<T> convertTo(String value) {
        checkNotBlank(value);

        try{
            var values = value.split(CSV_SEPARATOR);
            return Arrays.stream(values).map(delegate::convertTo).collect(Collectors.toList());
        }catch (Exception ex){
            throw new ConversionException(value, getType(), ex);
        }
    }

    @Override
    public String convertFrom(List<T> value) {
        checkNotNull(value);
        return value.stream().map(delegate::convertFrom).collect(Collectors.joining(CSV_SEPARATOR));
    }
}
