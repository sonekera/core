package com.guigui.common.converter.impl;

import com.guigui.common.converter.Converter;
import com.guigui.common.converter.exception.ConversionException;


import static com.guigui.common.util.PreconditionsUtil.checkNotBlank;
import static com.guigui.common.util.PreconditionsUtil.checkNotNull;

public class FloatConverter implements Converter<Float> {
    @Override
    public Class<Float> getType() {
        return Float.class;
    }

    @Override
    public Float convertTo(String value) {
        checkNotBlank(value);

        try{
            return Float.valueOf(value);
        }catch (Exception ex){
            throw new ConversionException(value, getType(), ex);
        }
    }

    @Override
    public String convertFrom(Float value) {
        return checkNotNull(value).toString();
    }
}
