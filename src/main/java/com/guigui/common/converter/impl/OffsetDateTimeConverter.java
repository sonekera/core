package com.guigui.common.converter.impl;

import com.guigui.common.converter.Converter;
import com.guigui.common.converter.exception.ConversionException;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

import static com.guigui.common.util.PreconditionsUtil.checkNotBlank;
import static com.guigui.common.util.PreconditionsUtil.checkNotNull;

public class OffsetDateTimeConverter implements Converter<OffsetDateTime> {
    @Override
    public Class<OffsetDateTime> getType() {
        return OffsetDateTime.class;
    }

    @Override
    public OffsetDateTime convertTo(String value) {
        checkNotBlank(value);

        try{
            return OffsetDateTime.parse(value, DateTimeFormatter.ISO_OFFSET_TIME);
        }catch (Exception ex){
            throw new ConversionException(value, getType(), ex);
        }
    }

    @Override
    public String convertFrom(OffsetDateTime value) {
        return checkNotNull(value).toString();
    }
}
