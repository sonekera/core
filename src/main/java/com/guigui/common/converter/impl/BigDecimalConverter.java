package com.guigui.common.converter.impl;

import com.guigui.common.converter.Converter;
import com.guigui.common.converter.exception.ConversionException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

import static com.guigui.common.util.PreconditionsUtil.checkNotBlank;
import static com.guigui.common.util.PreconditionsUtil.checkNotNull;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BigDecimalConverter implements Converter<BigDecimal> {
    @Override
    public Class<BigDecimal> getType() {
        return BigDecimal.class;
    }

    @Override
    public BigDecimal convertTo(String value) {
        checkNotBlank(value);
        try{
            return new BigDecimal(value);
        }catch (Exception ex){
            throw new ConversionException(value, getType(), ex);
        }
    }

    @Override
    public String convertFrom(BigDecimal value) {
        return checkNotNull(value).toString();
    }
}
