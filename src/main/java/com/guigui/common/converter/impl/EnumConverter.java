package com.guigui.common.converter.impl;

import com.guigui.common.converter.Converter;
import com.guigui.common.converter.exception.ConversionException;
import lombok.RequiredArgsConstructor;

import static com.guigui.common.util.PreconditionsUtil.checkNotBlank;
import static com.guigui.common.util.PreconditionsUtil.checkNotNull;

@RequiredArgsConstructor
public class EnumConverter<T extends Enum<T>> implements Converter<T> {

    private final Class<T> type;

    public static <T extends Enum<T>> EnumConverter<T> of(Class<T> type){
        return new EnumConverter<T>(type);
    }
    @Override
    public Class<T> getType() {
        return type;
    }

    @Override
    public T convertTo(String value) {
        checkNotBlank(value);
        try{
            return Enum.valueOf(type, checkNotBlank(value).toUpperCase());
        }catch (Exception ex){
            throw new ConversionException(value, getType(), ex);
        }
    }

    @Override
    public String convertFrom(T value) {
        return checkNotNull(value).toString();
    }
}
