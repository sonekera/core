package com.guigui.common.converter.impl;

import com.guigui.common.converter.Converter;
import com.guigui.common.converter.exception.ConversionException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;


import java.net.URI;

import static com.guigui.common.util.PreconditionsUtil.checkNotBlank;
import static com.guigui.common.util.PreconditionsUtil.checkNotNull;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class URIConverter implements Converter<URI> {
    
    @Override
    public Class<URI> getType() {
        return URI.class;
    }

    @Override
    public URI convertTo(String value) {
        checkNotBlank(value);

        try{
            return URI.create(value);
        }catch (Exception ex){
            throw new ConversionException(value, getType(), ex);
        }
    }

    @Override
    public String convertFrom(URI value) {
        return checkNotNull(value).toString();
    }
}
