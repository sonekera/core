package com.guigui.common.converter.impl;

import com.guigui.common.converter.Converter;
import com.guigui.common.converter.exception.ConversionException;

import static com.guigui.common.util.PreconditionsUtil.checkNotBlank;
import static com.guigui.common.util.PreconditionsUtil.checkNotNull;

public class DoubleConverter implements Converter<Double> {
    @Override
    public Class<Double> getType() {
        return Double.class;
    }

    @Override
    public Double convertTo(String value) {
        checkNotBlank(value);
        try{
            return Double.valueOf(value);
        }catch (Exception ex){
            throw new ConversionException(value, getType(), ex);
        }
    }

    @Override
    public String convertFrom(Double value) {
        return checkNotNull(value).toString();
    }
}
