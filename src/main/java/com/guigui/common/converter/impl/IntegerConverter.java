package com.guigui.common.converter.impl;

import com.guigui.common.converter.Converter;
import com.guigui.common.converter.exception.ConversionException;

import static com.guigui.common.util.PreconditionsUtil.checkNotBlank;
import static com.guigui.common.util.PreconditionsUtil.checkNotNull;

public class IntegerConverter implements Converter<Integer> {
    @Override
    public Class<Integer> getType() {
        return Integer.class;
    }

    @Override
    public Integer convertTo(String value) {
        checkNotBlank(value);

        try{
            return Integer.valueOf(value);
        }catch (Exception ex){
            throw new ConversionException(value, getType(), ex);
        }
    }

    @Override
    public String convertFrom(Integer value) {
        return checkNotNull(value).toString();
    }
}
