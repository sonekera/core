package com.guigui.common.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PreconditionsUtil {

    public static <T> T checkNotNull(T value) {
        if (value == null) {
            //TODO: Criar exceção customizada
            throw new RuntimeException("Não pode ser nulo");
        }
        return value;
    }

    public static String checkNotBlank(String value) {
        if (value == null || value.isBlank()) {
            throw new RuntimeException("Não pode ser branco");
        }
        return value;
    }
}